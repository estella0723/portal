/**
 * @description get api Data
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 10/05/2018
 */

import axios from 'axios';
import qs from 'qs';
const utils = require('../utils');


// axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.API_ROOT;

let getStatusUrl = {
  "wait-to-confirm": "/get-wait-confirm-po-list",
  "confirmed": "/get-confirmed-po-list",
  "wait-to-ship-WH": "/get-wait-to-ship-po-list",
  "wait-to-ship-CX": "/get-wait-to-ship-cx-po-list",
  "View-Warehouse-Shipped": "/get-shipped-po-list",
  "View-Customer-Shipped": "/get-cx-shipped-list",
  "invoice": "/get-invoice-list"
};

/**
 * Get captcha
 *
 * done
 */
export const getCaptcha = () => axios({
  method: 'get',
  url: '/get-captcha',
  emulateJSON: true
});

/**
 * login
 *
 * done
 */
export const login = (username, password, captcha, rememberMe) => axios({
  method: 'post',
  url: '/login',
  data: qs.stringify({username, password, captcha})
});

/**
 * Logout
 *
 * done
 */
export const logout = (token) => axios({
  method: 'post',
  url: '/logout',
  data: qs.stringify({token})
});

/**
 * Password Reset
 */
export const passwordReset = (username, password, captcha) => axios({
  method: 'post',
  url: '/reset-password',
  data: qs.stringify({username, password, captcha})
});

/**
 * Email Reset Password
 */
export const emailReset = (email) => axios({
  method: 'post',
  url: '/sent-reset-password-email',
  data: qs.stringify({email})
});

/**
 * supplier-PO wait-to-confirm
 * status: wait-to-confirm / confirmed / wait to ship / view warehouse shipped/ view customer shipped
 */
export const getSupplierPO = (status, data) => axios({
  method: 'post',
  url: getStatusUrl[status],
  data: qs.stringify(data)
}).catch(err => {
  // console.log(qs.stringify({token}));
});

/**
 * supplier-PO wait-to-confirm submit confirmed data
 */
export const submitWaitToConfirm = (confirmList, token) => axios({
  method: 'post',
  url: '/submit-wait-confirm-po-list',
  data: qs.stringify({confirmList, token})
});

/**
 * supplier-PO wait-to-confirm export excel
 *
 * done
 */
export const waitToConfirmExcelExport = (tags, token) => axios({
  method: 'post',
  url: '/download-wait-confirm-po-list',
  data: qs.stringify({token, tags}),
  responseType: 'blob'
});

/**
 * supplier-PO wait-to-ship submit confirmed data
 */
export const submitWaitToShip = (confirmedData) => axios({
  method: 'post',
  url: '/submit-wait-to-ship-po-list',
  data: qs.stringify({confirmedData})
});

/**
 * supplier-PO wait-to-ship export excel
 */
export const waitToShipExcelExport = (tags, token) => axios({
  method: 'post',
  url: '/download-wait-to-ship-po-list',
  data: qs.stringify({token, tags}),
  responseType: 'arraybuffer'
});

/**
 * supplier-PO create shipment
 */
export const preCheckWaitToShip = (confirmList, token) => axios({
  method: 'post',
  url: '/pre-check-wait-to-ship-po',
  data: qs.stringify({token, confirmList})
}).catch(err => {
});


/**
 * supplier-PO customer shipment create confirm
 */
export const createShipment = (confirmList, shipmentInfo, token) => axios({
  method: 'post',
  url: '/submit-wait-to-ship-po-list',
  data: qs.stringify({confirmList, shipmentInfo, token})
});

/**
 * supplier-PO wait-to-ship remove invoice
 */
export const removeInvoiceAttachment = (token, fileName, folderName) => axios({
  method: 'post',
  url: '/remove-invoice-attachment',
  data: qs.stringify({token, fileName, folderName})
});

/**
 * supplier-PO wait-to-ship download invoice
 */
export const downloadInvoice = (token, fileName, folderName) => axios({
  method: 'get',
  url: '/download-invoice-attachment/token/' + token + '/fileName/' + fileName + '/folderName/' + folderName
  // params: qs.stringify({token, fileName, folderName}),
  // responseType: 'blob'
});

/**
 * supplier-PO shipment detail
 */
export const shipmentDetail = (token,shipmentId) => axios({
  method: 'post',
  url: '/get-shipped-po-item-list',
  data: qs.stringify({token, shipmentId})
});

/**
 * supplier-PO shipment edit submit
 */
export const shipmentEdit = (token, shipmentInfo) => axios({
  method: 'post',
  url: '/update-shipment',
  data: qs.stringify({token, shipmentInfo})
});

/**
 * supplier-PO shipped search & get data
 */
export const searchShippedItems = (shipment) => axios({
  method: 'post',
  url: '/get-shipped-po-list',
  data: qs.stringify(shipment)
});


/**
 * supplier-PO customer create shipment
 *
 * API Name: getCxShipmentPendingCreateList
 *
 */
export const preCheckWaitToShipCx = (poInfo, token) => axios({
  method: 'post',
  url: '/get-cx-shipment-pending-create-list',
  data: qs.stringify({token, poInfo})
}).catch(err => {
});

/**
 *
 * page -> wait to ship
 *
 * supplier-PO customer create shipment confirm
 *
 * API Name: submitCxShipmentCreateList
 *
 */
export const submitCxShipmentCreate = (confirmData) => axios({
  method: 'post',
  url: '/submit-cx-shipment-create-list',
  data: qs.stringify(confirmData)
}).catch(err => {
});

/**
 * page -> shipped
 *
 * supplier-PO customer Edit shipment
 *
 * API Name: getCxShipmentItem
 *
 */
export const getCxShipment = (token, shipmentId) => axios({
  method: 'post',
  url: '/get-cx-shipment-item',
  data: qs.stringify({token, shipmentId})
}).catch(err => {
});


/**
 * page -> shipped
 *
 * supplier-PO customer create invoice
 *
 * API Name: getCreateInvoiceData
 *
 * params token: String, shipmengId: Array
 *
 */
export const getInvoiceData = (token, shipmentId) => axios({
  method: 'post',
  url: '/get-create-invoice-data',
  data: qs.stringify({token, shipmentId})
});

/**
 *
 * supplier-PO customer submit create invoice
 *
 * API Name: createInvoice
 *
 * params token: String, shipmengId: Array, invoiceNo: String, invoiceDate: String
 *
 */
export const createInvoice = (token, shipmentId, invoiceNo, invoiceDate, invoiceId) => axios({
  method: 'post',
  url: '/create-invoice',
  data: qs.stringify({token, shipmentId, invoiceNo, invoiceDate, invoiceId})
});

/**
 *
 * supplier-PO customer get invoice item
 *
 * API Name: getInvoiceItem
 *
 *
 */
export const getInvoiceItem = (token, invoiceId) => axios({
  method: 'post',
  url: '/get-invoice-item',
  data: qs.stringify({token, invoiceId})
});

export const editCxShipment = (token, shipmentId, fulfillment, supplierCourier, shippingFee, dispatchDate, trackingNo) => axios({
  method: 'post',
  url: '/edit-cx-shipment',
  data: qs.stringify({token, shipmentId, fulfillment, supplierCourier, shippingFee, dispatchDate, trackingNo})
});

export const editInvoice = (token, invoiceId, invoiceNo, invoiceDate) => axios({
  method: 'post',
  url: '/edit-invoice',
  data: qs.stringify({token, invoiceId, invoiceNo, invoiceDate})
});






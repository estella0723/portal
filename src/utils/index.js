/**
 * @description Description
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 2018/7/25
 */
/**
 * 储存localStorage
 */
export const setStore = (name, content) => {
  if(!name) return;
  content = JSON.stringify(content);
  window.localStorage.setItem(name, content);
};
/**
 * 获取localStorage
 */
export const getStore = (name) => {
  if(!name) return;
  return JSON.parse(window.localStorage.getItem(name));
};
/**
 * 移除localStorage
 */
export const removeStore = (name) => {
  if(!name) return;
  window.localStorage.removeItem(name);
};

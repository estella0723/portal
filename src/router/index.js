import Vue from 'vue';
import Router from 'vue-router';

const Page404 = () => import('@/pages/404/404');
const PageError = () => import('@/pages/error/error');
const Login = () => import('@/pages/login/login');
const ForgotPassword = () => import('@/pages/forgot-password/forgot-password');
const Main = () => import('@/pages/main/main');
const Dashboard = () => import('@/pages/main/dashboard/dashboard');
const AccountSet = () => import('@/pages/main/account/account-set');
const SupplierPO = () => import('@/pages/main/supplier-PO/supplier-PO');
const WaitToConfirm = () => import('@/pages/main/supplier-PO/wait-to-confirm/wait-to-confirm');
const Confirmed = () => import('@/pages/main/supplier-PO/confirmed/confirmed');
const WHIndex = () => import(/* webpackChunkName: "wait-to-ship-wh" */'@/pages/main/supplier-PO/wait-to-ship/warehouse/whIndex');
const WaitToShipWH = () => import(/* webpackChunkName: "wait-to-ship-wh" */'@/pages/main/supplier-PO/wait-to-ship/warehouse/wait-to-ship-WH');
const WaitToShipWHCreateShipment = () => import(/* webpackChunkName: "wait-to-ship-wh" */'@/pages/main/supplier-PO/wait-to-ship/warehouse/create-shipment.vue');
const CSRIndex = () => import(/* webpackChunkName: "wait-to-ship-cx" */'@/pages/main/supplier-PO/wait-to-ship/customer/customer-index');
const WaitToShipCSR = () => import(/* webpackChunkName: "wait-to-ship-cx" */'@/pages/main/supplier-PO/wait-to-ship/customer/wait-to-ship-CSR');
const WaitToShipCSRCreateShipment = () => import(/* webpackChunkName: "wait-to-ship-cx" */'@/pages/main/supplier-PO/wait-to-ship/customer/create-shipment.vue');
const ShippedIndex = () => import('@/pages/main/supplier-PO/shipped/shippedIndex');
const Shipped = () => import('@/pages/main/supplier-PO/shipped/shipped');
const EditWHShipment = () => import('@/pages/main/supplier-PO/shipped/warehouse/edit-shipment');
const WHShipmentDetail = () => import('@/pages/main/supplier-PO/shipped/warehouse/shipment-detail');
const EditCSRShipment = () => import('@/pages/main/supplier-PO/shipped/customer/edit-shipment');
const CSRShipmentDetail = () => import('@/pages/main/supplier-PO/shipped/customer/shipment-detail');
const CreateInvoice = () => import('@/pages/main/supplier-PO/shipped/customer/create-invoice');
const InvoiceIndex = () => import(/* webpackChunkName: "view-cx-invoice" */'@/pages/main/supplier-PO/shipped/customer/view-invoice/invoiceIndex');
const ViewInvoice = () => import(/* webpackChunkName: "view-cx-invoice" */'@/pages/main/supplier-PO/shipped/customer/view-invoice/view-invoice');
const EditInvoice = () => import(/* webpackChunkName: "view-cx-invoice" */'@/pages/main/supplier-PO/shipped/customer/view-invoice/edit-invoice');
const ViewInvoiceDetail = () => import(/* webpackChunkName: "view-cx-invoice" */'@/pages/main/supplier-PO/shipped/customer/view-invoice/view-invoice-detail');


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "*",
      redirect: "/404",
    },
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/404',
      name: 'Page404',
      component: Page404
    },
    {
      path: '/error',
      name: 'PageError',
      component: PageError
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: ForgotPassword
    },
    {
      path: '/main',
      name: 'Main',
      redirect: '/dashboard',
      component: Main,
      children: [
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: '/account-set',
          name: 'Account Set',
          component: AccountSet
        },
        {
          path: '/supplier-PO',
          name: 'Supplier PO',
          component: SupplierPO,
          redirect: '/supplier-PO/wait-to-confirm',
          children: [
            {
              path: 'wait-to-confirm',
              name: 'Wait To Confirm',
              component: WaitToConfirm,
            },
            {
              path: 'confirmed',
              name: 'Confirmed',
              component: Confirmed,
            },
            {
              path: 'wait-to-ship/warehouse',
              component: WHIndex,
              props: {name: 'Wait To Ship(Warehouse)'},
              children: [
                {
                  path: '',
                  name: 'waitToShipWHIndex',
                  component: WaitToShipWH
                },
                {
                  path: 'create-shipment',
                  name: 'Warehouse Shipment Create',
                  component: WaitToShipWHCreateShipment
                }
              ]
            },
            {
              path: 'wait-to-ship/customer',
              component: CSRIndex,
              props: {name: 'Wait To Ship(Customer)'},
              meta: { role: ['dropShipping_supplier'] },
              children: [
                {
                  path: '',
                  meta: { role: ['dropShipping_supplier'] },
                  name: 'waitToShipCSRIndex',
                  component: WaitToShipCSR
                },
                {
                  path: 'create-shipment',
                  meta: { role: ['dropShipping_supplier'] },
                  name: 'Customer Shipment Create',
                  component: WaitToShipCSRCreateShipment
                }
              ]
            },
            {
              path: 'shipped/:type',
              component: ShippedIndex,
              props: {name: 'Shipped'},
              children: [
                {
                  path: '',
                  name: 'ShippedIndex',
                  component: Shipped
                },
                {
                  path: 'edit-shipment-WH',
                  name: 'Edit Warehouse Shipment',
                  component: EditWHShipment
                },
                {
                  path: 'edit-shipment-CSR',
                  meta: { role: ['dropShipping_supplier'] },
                  name: 'Edit Customer Shipment',
                  component: EditCSRShipment
                },
                {
                  path: 'shipment-detail-WH',
                  name: 'Warehouse Shipment Detail',
                  component: WHShipmentDetail
                },
                {
                  path: 'shipment-detail-CSR',
                  meta: { role: ['dropShipping_supplier'] },
                  name: 'Customer Shipment Detail',
                  component: CSRShipmentDetail
                },
                {
                  path: 'create-invoice',
                  meta: { role: ['dropShipping_supplier'] },
                  name: 'Create Invoice',
                  component: CreateInvoice
                },
                {
                  path: 'view-invoice',
                  meta: { role: ['dropShipping_supplier'] },
                  component: InvoiceIndex,
                  props: {name: 'View Invoice'},
                  children: [
                    {
                      path: '',
                      name: 'invoiceIndex',
                      meta: { role: ['dropShipping_supplier'] },
                      component: ViewInvoice
                    },
                    {
                      path: 'edit-invoice',
                      name: 'Edit Invoice',
                      meta: { role: ['dropShipping_supplier'] },
                      component: EditInvoice
                    },
                    {
                      path: 'view-invoice-detail',
                      name: 'View Invoice Detail',
                      meta: { role: ['dropShipping_supplier'] },
                      component: ViewInvoiceDetail
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ]
});


/**
 * @description Description
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 2018/7/25
 */

import {
  GET_USERINFO,
  LOGOUT,
  GET_CREATESHIP,
  SET_FILTER,
  GET_INVOICEDATA
} from './mutation-types';


export default {
  [GET_USERINFO](state, userInfo) {
    state.userInfo = userInfo;
  },
  [LOGOUT](state) {
    state.userInfo = {
      token: '',
      currencyUnit: '',
      userName: '',
      loginTime: ''
    };
  },
  // record create ship item list
  [GET_CREATESHIP](state, data) {
    state.createShipmentData = data;
  },
  [SET_FILTER] (state, filter) {
    filter.page ? filter.page = parseInt(filter.page) : null;
    filter.perPage ? filter.perPage = parseInt(filter.perPage) : null;
    typeof filter.status == 'string'? filter.status = filter.status.split(","): null;
    state.filter = filter;
  },
  [GET_INVOICEDATA] (state, createInvoiceData) {
    state.createInvoiceData = createInvoiceData;
  }
}

/**
 * @description Actions
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 2018/7/25
 *
 * @rewrite Estella
 * use actions as outer interface instead of mutations
 * actions deal with user's state action, and call mutations to update store
 *
 */
import {getUser} from '@/api/index';
import {GET_USERINFO, LOGOUT, GET_CREATESHIP, GET_INVOICEDATA} from './mutation-types';
import {setStore, getStore, removeStore} from '@/utils/index';


export default {
  setUserInfo({commit}, userInfo) {
    setStore('userInfo', {
      userName: userInfo.userName,
      token: userInfo.token,
      currencyUnit: userInfo.currency ? userInfo.currency : '$',
      loginTime: userInfo.loginTime,
      dropShippingSupplier: true
    });
    commit('GET_USERINFO', getStore('userInfo'));
  },
  getUserInfo({commit}) {
    commit('GET_USERINFO', getStore('userInfo'));
  },
  logOut({commit}) {
    window.localStorage.clear();
    commit('LOGOUT');
  },
  getCreateShipmentData({commit}) {
    commit('GET_CREATESHIP', getStore('shipmentData'));
  },
  setCreateShipmentData({commit}, createShipmentData) {
    setStore('shipmentData', createShipmentData);
    commit('GET_CREATESHIP', getStore('shipmentData'));
  },
  setFilter({commit}, filter) {
    commit('SET_FILTER', filter);
  },
  setInvoiceData({commit}, createInvoiceData) {
    setStore('invoiceData', createInvoiceData);
    commit('GET_INVOICEDATA', createInvoiceData);
  },
  getInvoiceData({commit}) {
    commit('GET_INVOICEDATA', getStore('invoiceData'));
  }
}

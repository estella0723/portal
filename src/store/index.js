/**
 * @description Description
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 2018/7/25
 */
import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import actions from './action';


const state = {
  userInfo: {
    token: '',
    currencyUnit: '',
    userName: '',
    loginTime: '',
    dropShippingSupplier: false
  },
  createShipmentData: {},
  filter: {},
  createInvoiceData: {
    invoiceNo: '',
    invoiceDate: null,
    attachment: {
      folderName: null,
      fileList: []
    },
    shipmentIdList: []
  }
};


Vue.use(Vuex);
export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters: {
    dropShippingSupplier: (state) => {
      return state.userInfo.dropShippingSupplier
    }
  }
})

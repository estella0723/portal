/**
 * @description Description
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 2018/7/25
 */
export const GET_USERINFO = 'GET_USERINFO';
export const LOGOUT = 'LOGOUT';
export const GET_CREATESHIP = 'GET_CREATESHIP';
export const SET_FILTER = 'SET_FILTER';
export const GET_INVOICEDATA = 'GET_INVOICEDATA';

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import iView from 'iview';
import 'iview/dist/styles/iview.css';

import App from './App';
import store from './store/index';
import router from './router/index';
import axios from 'axios';



// configure language
import lang from 'iview/dist/locale/en-US';
// import './reset/index.less';

iView.locale(lang);

Vue.use(iView);
process.env.qs && require('qs');

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  iView.LoadingBar.start();
  iView.Spin.show();
  iView.Modal.remove();
  document.body.removeAttribute('style');
  document.documentElement.scrollTop = document.body.scrollTop = 0;

  if (!window.localStorage.userInfo) {
    if (to.name != 'Login') {
      next({
        path: '/login',
        query: {redirect: to.fullPath}//登录成功后跳入浏览的当前页面
      });
    } else {
      next();
    }
  } else if (window.localStorage.userInfo) {
    store.dispatch('getUserInfo');
    if (to.name == 'Login') {
      next({path: '/main'});
    } else {
      next();
    }
  }
});

router.afterEach(route => {
  iView.LoadingBar.finish();
  iView.Spin.hide();
});

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          // 这里写清除token的代码
          console.log(router.currentRoute.fullPath);
          window.localStorage.removeItem('userInfo');
          router.replace({
            path: '/login',
            query: {redirect: router.currentRoute.fullPath}//登录成功后跳入浏览的当前页面
          });
          this.$Message.info(error.response.data.message);
          break;
      }
    }
    return Promise.reject(error.response.data)
  });

new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
});

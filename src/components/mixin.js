/**
 * @description Description
 *
 * @author Candy <candy.yan@motionglobal.com>
 * @date  Creation Date 2018/7/27
 */

import crypto from 'crypto';
import router from '../router/index';



export const siderMenu = {
  methods: {
    curMenuSelected(curRouter) {
      let activeName, openNames;
      curRouter = curRouter.path;
      switch (true) {
        case curRouter.indexOf('/dashboard') >= 0:
          activeName = '1';
          openNames = ['1'];
          break;
        case curRouter.indexOf('/account-set') >= 0:
          activeName = '2-1';
          openNames = ['2'];
          break;
        case curRouter.indexOf('/supplier-PO/wait-to-confirm') >= 0:
          activeName = '3-1';
          openNames = ['3'];
          break;
        case curRouter.indexOf('/supplier-PO/confirmed') >= 0:
          activeName = '3-2';
          openNames = ['3'];
          break;
        case curRouter.indexOf('/supplier-PO/wait-to-ship/warehouse') >= 0:
          activeName = '3-3';
          openNames = ['3'];
          break;
        case curRouter.indexOf('/supplier-PO/wait-to-ship/customer') >= 0:
          activeName = '3-4';
          openNames = ['3'];
          break;
        case curRouter.indexOf('/supplier-PO/shipped') >= 0:
          activeName = '3-5';
          openNames = ['3'];
          break;
        default:
          activeName = '1-1';
          openNames = '1';
          break;
      }
      return {
        activeName,
        openNames
      }
    },
    goToPath(name) {
      switch (name) {
        case '1-1':
          router.push('/dashboard');
          break;
        case '2-1':
          router.push('/account-set');
          break;
        case '3-1':
          router.push('/supplier-PO/wait-to-confirm');
          break;
        case '3-2':
          router.push('/supplier-PO/confirmed');
          break;
        case '3-3':
          router.push('/supplier-PO/wait-to-ship/warehouse');
          break;
        case '3-4':
          router.push('/supplier-PO/wait-to-ship/customer');
          break;
        case '3-5':
          router.push({
            name: 'ShippedIndex',
            params: {type: typeof router.params == 'undefined' ? 'View-Warehouse-Shipped' : router.params.type}
          });
          break;
        default:
          router.push('/dashboard');
          break;
      }
    }
  }
};

export const modalInstance = {
  methods: {
    modalInstance(type, title, content) {
      switch (type) {
        case 'info':
          this.$Modal.info({
            title: title,
            content: content
          });
          break;
        case 'success':
          this.$Modal.success({
            title: title,
            content: content
          });
          break;
        case 'warning':
          this.$Modal.warning({
            title: title,
            content: content
          });
          break;
        case 'error':
          this.$Modal.error({
            title: title,
            content: content
          });
          break;
      }
    }
  }
};

export const getmd5 = {
  methods: {
    getmd5(password) {
      var md5 = crypto.createHash("md5");
      md5.update(password);
      password = md5.digest('hex');
      return password;
    }
  }
}
